#pragma once

#ifndef _SENSORHANDLER_H
#define _SENSORHANDLER_H

const byte greenPin = 11;
const byte yellowPin = 12;
const byte redPin = 10;

const byte windowPin = A0;
const byte doorPin = A1;

int gLastSensor1 = 0;
int gLastSensor2 = 0;
int gLastState = 1;

//const byte beepPin = 13;
//BEEP already defined


//stub
void sensorSetup();
void sensorAttach();
int sensorHandler(int pin, int &lastSensorF, int &lastStateF);

#endif