#pragma once

#ifndef _KEYHANDLER_H
 #define _KEYHANDLER_H

const byte BEEP = 13;

const byte COL0 = 9;
const byte COL1 = 8;
const byte COL2 = 7;
const byte COL3 = 6;

const byte ROW0 = 5;
const byte ROW1 = 4;
const byte ROW2 = 3;
const byte ROW3 = 2;

MultitapKeypad kpd;

//stub
void keySetup();
void keyHandler();

#endif


