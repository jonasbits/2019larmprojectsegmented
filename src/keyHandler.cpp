#include <Arduino.h>
#include <MultitapKeypad.h>
#include "keyHandler.h"
#include "sensorHandler.h"

// creates kpd as MultitapKeypad object
// for matrix 4 x 3 keypad
// MultitapKeypad kpd( ROW0, ROW1, ROW2, ROW3, COL0, COL1, COL2 );
// for matrix 4 x 4 keypad

MultitapKeypad kpd( ROW0, ROW1, ROW2, ROW3, COL0, COL1, COL2, COL3 );
//library takes care of pinMODE init for me

// creates key as Key object
Key key;

void keySetup(){
  kpd.attachFunction(sensorAttach);
}

void keyHandler(MultitapKeypad kpdF) {

  key = kpdF.getKey();

  Serial.print( F( "Key " ) );
  
  if ( key.character > 0 )
    Serial.print( char( key.character ) );
  else
    Serial.print( F( "??" ) );

  switch ( key.state ) {
    /* KEY_DOWN KEY_DOWN KEY_DOWN KEY_DOWN */
    case KEY_DOWN:
      //do nothing

    /* MULTI_TAP MULTI_TAP MULTI_TAP MULTI_TAP */
    case MULTI_TAP:
      tone( BEEP, 5000, 20 );
      Serial.println( F( " down" ) );
      if ( key.state == MULTI_TAP ) {
        Serial.print( F( "TapCounter: " ) );
        if ( key.tapCounter < 10 )
          Serial.println( key.tapCounter, DEC );
        else {
          kpd.resetTapCounter();
          Serial.println( 0 );
        }
      }
      break;
    
    /* LONG_TAP LONG_TAP LONG_TAP LONG_TAP */
    case LONG_TAP:
      tone( BEEP, 5000, 20 );
      Serial.println( F( " hold" ) );
      break;
    
    /* MULTI_KEY_DOWN MULTI_KEY_DOWN MULTI_KEY_DOWN */
    case MULTI_KEY_DOWN:
      tone( BEEP, 4000, 100 );
      Serial.println( F( " down" ) );
      break;
    
    /* KEY_UP KEY_UP KEY_UP KEY_UP KEY_UP */
    case KEY_UP:
      Serial.println( F( " up" ) );
    
    /* CANCELED CANCELED CANCELED CANCELED*/
    case CANCELED:
      break; //do nothing

    /*
    case MULTI_KEY_DOWN || KEY_DOWN || MULTI_TAP || LONG_TAP || CANCELED:
      //do nothing
    default:
      break;
    */
  
  } //end switch
}