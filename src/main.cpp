/*
 Key States Test on Serial Monitor
 
 
 Created 15 April 2017
 @Gorontalo, Indonesia
 by ZulNs
 */

#include <Arduino.h>
#include <MultitapKeypad.h>
#include "keyHandler.h"
#include "sensorHandler.h"

#define BAUD 9600
#define DEBUG

#ifndef DEBUG 
  //if not in DEBUG mode, be more anoying
  #define ALARMFREQ 3000
 #else
  //DEBUG mode make the noice bearable
  #define ALARMFREQ 1000
#endif

void setup() {
  sensorSetup();
  keySetup();

  Serial.begin(BAUD);

  Serial.println( F( "Key States Test on Serial Monitor" ) );
  Serial.println( F( "by ZulNs" ) );
  Serial.println();
  tone( BEEP, 4000, 50 );
  Serial.println();
  Serial.println( F( "Now you can test any key states..." ) );
  Serial.println();
}

void loop() {
  keyHandler(kpd);
}

