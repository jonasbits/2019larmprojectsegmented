#include <Arduino.h>
#include "sensorHandler.h"

void sensorSetup(){
// put your setup code here, to run once:
  pinMode(windowPin,INPUT); //window pin
  pinMode(doorPin,INPUT); //door pin
  pinMode(yellowPin,OUTPUT); //yellow
  pinMode(greenPin,OUTPUT); //green
  pinMode(redPin,OUTPUT); //red
}

void sensorAttach(){
    int lastSensor1 = gLastSensor1;
    int lastSensor2 = gLastSensor2;
    int lastState = gLastState;
    if ( sensorHandler(windowPin,lastSensor1,lastState) || sensorHandler(doorPin,lastSensor2,lastState) ) {
        Serial.print("#,"); 
        Serial.print(lastState); 
        Serial.print(","); 
        Serial.print(lastSensor1);
        Serial.print(","); 
        Serial.println(lastSensor2);
    }
    
}

int sensorHandler(int pin, int &lastSensorF, int &lastStateF) {
    int printLogChangeF = 0;
    //receive lastButtonF as referens, copy in/copy out.
    int nowSensor = digitalRead(pin);
    //int nowButton = analogRead(pin); //will change every rotation, do not use
  
    if (nowSensor < lastSensorF) {
        /* Serial.print("#,");
        Serial.print(lastStateF);
        Serial.println(",0,debug"); //check buttons
        */
        if (lastStateF > 0){
        lastStateF = 2; //raise alarm to noice level
        }

        if (lastStateF == -1 && pin == A1) {
        lastStateF = 2; //raise alarm to noice level, only on DOOR trigger
        }
        
        printLogChangeF = 1;
        //send to PC for logging
    }
  
    if (nowSensor > lastSensorF) {
        /* Serial.print("#,");
        Serial.print(lastStateF);
        Serial.println(",1,debug"); //check buttons
        */
        printLogChangeF = 1;
    }

    //only this line is important, for debounce
    lastSensorF = nowSensor;
    delay(10);
    return printLogChangeF;
}
